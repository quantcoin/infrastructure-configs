var WalletConfig = {
    "coreAsset": "QUANT",
    "apiServer": "ws://quantcoin.tk/witness_api/",
    "apiServersMap": [{"url": "ws://quantcoin.tk/witness_api/", "location": "Main Server"}],
    "faucetAddress": "http://quantcoin.tk/faucet/",
    "topMarkets": ["QUANT"]
}
